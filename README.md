# vue_echarts大屏

#### 介绍
vue加echarts写的一个大屏项目

#### 软件架构
软件架构说明


#### 安装教程

# install dependencies
cnpm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

#### 使用说明

主要文件介绍

文件	作用/功能
main.js	主目录文件，引入 Echart/DataV 等文件
utils	工具函数与 mixins 函数等
views/ index.vue	项目主结构
views/其余文件	界面各个区域组件（按照位置来命名）
assets	静态资源目录，放置 logo 与背景图片
assets / style.scss	通用 CSS 文件，全局项目快捷样式调节
assets / index.scss	Index 界面的 CSS 文件
components/echart	所有 echart 图表（按照位置来命名）
common/...	全局封装的 ECharts 和 flexible 插件代码（适配屏幕尺寸，可定制化修改）
案例演示：
### 一.首先全局路由设置默认跳转到index.vue页面
route/index.js页面

```
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
  path: '/',
  name: 'index',
  component: () => import('../views/index.vue')
}
  // {
  //   path: '/',
  //   component: () => import('../views/sellerPage.vue')
  // }

]
const router = new VueRouter({
  mode: "history",
  routes
})

export default router
```
### 二.在index页面绘制好整个页面的布局div，css样式布局这里不再赘述
### 三.新建vue组件，里面编写echarts的代码
    这里已饼状图为例：
components/echart/centreRight2/centreRighteChart2.vue

```
<template>
  <div>
    <div id="centreRight2Chart2" ></div>
  </div>
</template>

<script>


export default {
  data() {
    return {
      chart: null,
      newsList: null,
    };
  },
  mounted() {
    this.getData()
  },
  methods: {
    //近6个月接收量
    getData(){//这里是请求后台的数据
      this.axios.get('http://36.156.158.102:8085/cqdata/cqdata/querySumByMonth').then((response)=>{
        this.newsList = response.data.data;
        console.log('cc',response.data.data)
        this.draw()
      })
    },
    draw() {//这里是echarts图标的方法
    
      let month = this.newsList.map((item) =>{
        return item.receiveMonth
      })
      //先用set去重
      let set4 = new Set(month)
      //然后转成array
      //得到近6个月的月份,echarts的data里需要这种数组的格式
     //distinctMonth = ["2020-10","2020-11","2020-12","2021-1","2021-2","2023-3"]
      const distinctMonth = Array.from(set4)
      //得到6个月的垃圾船舶的数据
      const yval1 = this.newsList.filter(r => r.sewageType ==='船舶垃圾').map(r => r.receiveNum)
      //得到6个月的生活污水的数据
      const yval2 = this.newsList.filter(r => r.sewageType ==='生活污水').map(r => r.receiveNum)

      // 基于准备好的dom，初始化echarts实例
      this.chart = this.$echarts.init(document.getElementById("centreRight2Chart2"));
      let option = {
        //图标的提示框组件,鼠标放上去显示的一些提示信息
        tooltip: {
          trigger: "axis",
          axisPointer: {
            // 坐标轴指示器，坐标轴触发有效
            type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
          }
        },
        //网格容器,可以控制图表大小
        grid: {
          left: '4%',
          right: '2%',
          bottom: '12%',
          top: '4%',
          containLabel: true
        },
        //设置x轴的相关配置
        xAxis: {
          //修改刻度标签相关样式
          axisLabel: {
            interval: 0,//横轴信息显示
            color: "rgba(255,255,255,.9)"
          },
          //不显示x轴的线
          axisLine: {
            show: true,
          },
          data:distinctMonth, //6个月的月份数组数据
          crosshair: true,
          labels: {
            step: 1,
            staggerLines: 1,
          },
          tickLength: 3
        },
        //设置y轴的相关配置
        yAxis: {
          axisLine: {
            show: false,
          },
          axisLabel: {
            color: "rgba(255,255,255,.9)"
          },
          splitLine: {
            lineStyle: {
              color: "rgba(255,255,255,.1)"
            }
          },
          allowDecimals: false,
          gridLineColor: "#273E53",
          lineColor: "#273E53",
          lineWidth: 1,
        },
        // 图例组件
        legend: {
          orient: 'horizontal',      // 布局方式，默认为水平布局，可选为：
                                     // 'horizontal' ¦ 'vertical'
          x: 'center',               // 水平安放位置，默认为全图居中，可选为：
                                     // 'center' ¦ 'left' ¦ 'right'
                                     // ¦ {number}（x坐标，单位px）
          backgroundColor: 'rgba(0,0,0,0)',
          borderColor: '#ccc',       // 图例边框颜色
          borderWidth: 0,            // 图例边框线宽，单位px，默认为0（无边框）
          padding: 5,                // 图例内边距，单位px，默认各方向内边距为5，
                                     // 接受数组分别设定上右下左边距，同css
          itemGap: 10,               // 各个item之间的间隔，单位px，默认为10，
                                     // 横向布局时为水平间隔，纵向布局时为纵向间隔
          itemWidth: 10,             // 图例图形宽度
          itemHeight: 10,            // 图例图形高度
          bottom: '0%',               //下面距离
          textStyle: {
            color: 'white'          // 图例文字颜色
          }
        },

        //系列图标配置
        series: [
          {
            name: '船舶垃圾',
            type: 'bar',
            color: 'rgb(86, 137, 239)',//柱子的颜色
            barWidth: '35%',//柱子的宽度
            data: yval1  //船舶垃圾数组数据
          },
          {
            name: '生活污水',
            type: 'bar',
            // stack: '总量',
            color: 'rgb(85,206,160)',
            barWidth: '35%',//柱子的宽度
            data: yval2 //垃圾船舶数组数据
          },

        ],
      }

      this.chart.setOption(option);
    }
  },
  destroyed() {
    window.onresize = null;
  }
};
</script>

<style lang="scss" scoped>
#centreRight2Chart2 { //这里定义图标的大小，图标的大小根据提前绘制好的div决定
  width: 100%;
    height: 168px;
    margin: 0 auto;
}
</style>
```

### 四.在index.vue里引入写好的组件到对应的位置就行啦


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
